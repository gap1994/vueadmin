import request from '@/utils/request'

export function nav() {
  return request({
    url: 'sys/menu/nav',
    method: 'get'
  })
}

export function list() {
  return request({
    url: 'sys/menu/list',
    method: 'get'
  })
}

export function info(id) {
  return request({
    url: `/sys/menu/info/${id}`,
    method: 'get'
  })
}

export function select() {
  return request({
    url: '/sys/menu/select',
    method: 'get'
  })
}

export function saveOrUpdate(id, data) {
  return request({
    url: `/sys/menu/${!id ? 'save' : 'update'}`,
    method: 'post',
    data: data
  })
}

export function del(id) {
  return request({
    url: `/sys/menu/delete/${id}`,
    method: 'post'
  })
}
