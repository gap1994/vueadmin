import request from '@/utils/request'

export function info(id) {
  return request({
    url: `sys/schedule/info/${id}`,
    method: 'get'
  })
}

export function saveOrUpdate(id, data) {
  return request({
    url: `/sys/schedule/${!id ? 'save' : 'update'}`,
    method: 'post',
    data: data
  })
}

export function list(data) {
  return request({
    url: 'sys/schedule/list',
    method: 'get',
    params: data
  })
}

export function pause(ids) {
  return request({
    url: 'sys/schedule/pause',
    method: 'post',
    data: ids
  })
}

export function resume(ids) {
  return request({
    url: 'sys/schedule/resume',
    method: 'post',
    data: ids
  })
}

export function run(ids) {
  return request({
    url: '/sys/schedule/run',
    method: 'post',
    data: ids
  })
}

export function del(ids) {
  return request({
    url: 'sys/schedule/delete',
    method: 'post',
    data: ids
  })
}
