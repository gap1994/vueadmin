import request from '@/utils/request'

export function select() {
  return request({
    url: '/sys/role/select',
    method: 'get'
  })
}

export function list(data) {
  return request({
    url: '/sys/role/list',
    method: 'get',
    params: data
  })
}

export function info(id) {
  return request({
    url: `/sys/role/info/${id}`,
    method: 'get'
  })
}

export function saveOrUpdate(id, data) {
  return request({
    url: `/sys/role/${!id ? 'save' : 'update'}`,
    method: 'post',
    data: data
  })
}

export function del(ids) {
  return request({
    url: '/sys/role/delete',
    method: 'post',
    data: ids
  })
}
