import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/sys/login',
    method: 'post',
    data
  })
}

export function getInfo() {
  return request({
    url: '/sys/user/info',
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/sys/logout',
    method: 'post'
  })
}

export function password(data) {
  return request({
    url: '/sys/user/password',
    method: 'post',
    data
  })
}

export function list(data) {
  return request({
    url: '/sys/user/list',
    method: 'get',
    params: data
  })
}

export function info(id) {
  return request({
    url: `/sys/user/info/${id}`,
    method: 'get'
  })
}

export function saveOrUpdate(id, data) {
  return request({
    url: `/sys/user/${!id ? 'save' : 'update'}`,
    method: 'post',
    data
  })
}

export function del(userIds) {
  return request({
    url: '/sys/user/delete',
    method: 'post',
    data: userIds
  })
}
