import request from '@/utils/request'

export function list(data) {
  return request({
    url: 'sys/oss/list',
    method: 'get',
    params: data
  })
}

export function config() {
  return request({
    url: 'sys/oss/config',
    method: 'get'
  })
}
export function saveConfig(data) {
  return request({
    url: '/sys/oss/saveConfig',
    method: 'post',
    data: data
  })
}

export function del(ids) {
  return request({
    url: 'sys/oss/delete',
    method: 'post',
    data: ids
  })
}
